'use strict';
module.exports = (sequelize, DataTypes) => {
  const Speaker = sequelize.define('Speaker', {
    firstName: DataTypes.STRING
  });
  Speaker.associate = function(models) {
      models.Speaker.belongsTo(models.Company, {
          onDelete: "CASCADE",
          foreignKey:{
            allowNull: false
          }
      });
      Speaker.belongsTo(models.ConferenceSpeaker, {
        foreignKey: 'id',
        targetKey: 'speakerId'
      });
  };
  return Speaker;
};
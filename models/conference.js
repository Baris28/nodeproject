'use strict';
module.exports = (sequelize, DataTypes) => {
  const Conference = sequelize.define('Conference', {
    title: DataTypes.STRING,
    description: DataTypes.STRING
  }, {});
  Conference.associate = function(models) {
    Conference.belongsTo(models.ConferenceSpeaker, {
      foreignKey: 'id',
      targetKey: 'conferenceId'
    });
  };
  return Conference;
};
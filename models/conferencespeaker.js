'use strict';
module.exports = (sequelize, DataTypes) => {
  const ConferenceSpeaker = sequelize.define('ConferenceSpeaker', {
    conferenceId: DataTypes.INTEGER,
    speakerId: DataTypes.INTEGER
  }, {});
  ConferenceSpeaker.associate = function(models) {
    ConferenceSpeaker.hasMany(models.Conference, {
      foreignKey: 'id',
      sourceKey: 'conferenceId'
    });
    ConferenceSpeaker.hasMany(models.Speaker, {
      foreignKey: 'id',
      sourceKey: 'speakerId'
    });
  };
  return ConferenceSpeaker;
};
'use strict';
module.exports = (sequelize, DataTypes) => {
  const Company = sequelize.define('Company', {
    name: DataTypes.STRING
  });
  Company.associate = function(models) {
    models.Company.hasMany(models.Speaker);
  };
  return Company;
};
const conferenceController = require('../controllers/conference');

const express = require('express');

const router = express.Router();

//all Conferences
router.get('/', conferenceController.getAllConference);

//One Conference
router.get('/show/:conferenceId', conferenceController.getConference);

// Add Conference
router.get('/add-conference', conferenceController.getAddConference);
router.post('/add-conference', conferenceController.postStoreConference);

// update Conference
router.get('/edit/:conferenceId', conferenceController.getUpdateConference);
router.post('/edit/:conferenceId', conferenceController.postUpdateConference);

// delete Conference
router.post('/destroy/:confId', conferenceController.postDestroyConference);

module.exports = router;
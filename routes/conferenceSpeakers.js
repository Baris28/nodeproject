const conferenceSpeakerController = require('../controllers/conferenceSpeaker');

const express = require('express');

const router = express.Router();

//all Conference speakers
router.get('/',conferenceSpeakerController.getAllConferenceSpeakers);  // many to many 



module.exports = router;
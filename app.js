//Node modules
const path = require('path');
const bodyParser = require('body-parser');

//Middleware
const express = require('express');

const app = express();

//import routes
const speakerRoutes = require('./routes/speaker');
const conferenceRoutes = require('./routes/conference');
const conferenceSpeakerRoutes = require('./routes/conferenceSpeakers');

// view engine setup
app.set('view engine', 'ejs');

app.use(bodyParser.urlencoded( // routes 7-12
    {extended: false})
);

//static files
app.use(express.static(path.join(__dirname,'public')));

// routes
app.use('/speakers', speakerRoutes);
app.use('/conference', conferenceRoutes);
app.use('/conferencespeaker', conferenceSpeakerRoutes);

// home page
app.get('/', (req, res, next) => {
    res.render('index', {
        title: 'Conference App',
        description: 'What can you do here?'
    });
});

// 404 error 
app.use((req, res, next) => {
    res.status(404).render('404', {
        title: 404
    });
    console.log('app.js 404');
});

app.listen(3000);

models = require('./../models');


// many to many
exports.getAllConferenceSpeakers = ('/', (req, res) => {
    models.ConferenceSpeaker.findAll({
        include: [models.Conference, models.Speaker]
    })
        .then(result => {
            res.render('conferenceSpeaker/index', { result: result });
        })
});

models = require('./../models');

// get all conferences
exports.getAllConference = (req, res, next) => {
    models.Conference.findAll().then(function (conferences) {
        res.render('conference/index', {
            conferences: conferences,
        });
    })
};

// get one conference
exports.getConference = (req, res, next) => {
    const conferenceId = req.params.conferenceId;
    models.Conference.findByPk(conferenceId)
        .then(conference => {
            res.render('conference/show', {
                path: '/conference',
                conference: conference
            });
        })
        .catch(err => {
            console.log(err);
        });
};

// get and post a conference
exports.getAddConference = (req, res, next) => {
    res.render('conference/addConference', {
        title: 'Maak een conference aan',
        description: 'Wat voor conference wil je registreren?'
    });
}

exports.postStoreConference = (req, res, next) => {
    const title = req.body.title;
    const description = req.body.description;
    models.Conference.create({
        title: title,
        description: description
    }).then(result => {
        console.log('Conference aangemaakt!');
    }).catch(err => {
        console.log(err);
    });
    res.redirect('/conference');
};

// get conference and update
exports.getUpdateConference = (req, res, next) => {
    const conferenceId = req.params.conferenceId;
    models.Conference.findByPk(conferenceId)
        .then(conference => {
            res.render('conference/edit', {
                path: '/conference',
                conference: conference
            });
        })
        .catch(err => {
            console.log(err);
        });
};

exports.postUpdateConference = (req, res, next) => {
    const id = req.params.conferenceId
    const title = req.body.title;
    const description = req.body.description;
    models.Conference.findByPk(id)
        .then(conference => {
            conference.update({
                title: title,
                description: description
            })
        }).then(result => {
            console.log('Conference updated!');
        }).catch(err => {
            console.log(err);
        });
    res.redirect('/conference');
};


// delete Conference
exports.postDestroyConference = (req, res, next) => {
    const confId = req.params.confId;
    models.Conference.findByPk(confId)
        .then(conference => {
            return conference.destroy();
        })
        .then(result => {
            console.log('Conference verwijderd!');
            res.redirect('/conference');
        })
        .catch(err => {
            console.log(err);
        });
};
models = require('./../models');
/* 
exports.getAllSpeakers = ('/', (req, res) => {
    models.Speaker.findAll().then(function (speakers) {
        res.render('speaker/index', {
            name: speakers.Company,
            speakers: speakers
        });
    });
}); */


// one to many
exports.getAllCompanySpeakers = ('/', (req, res) => {
    models.Speaker.findAll({
        include: [models.Company]
    })
        .then(result => {
            res.render('company/index', { result: result });
        })
}
);